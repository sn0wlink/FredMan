<html>

<head>
    <title><?php echo $SiteName; ?></title>
    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="style.php" />
    <meta http-equiv='Refresh' content='<?php echo $RefreshTime; ?>; url=index.php' />
</head>

<body>

<div class='menu'>
    <div class='logo'>
        <?php echo "$SiteName"; ?>
    </div>
        <a class='NewTicket' href='NewTicket.php'>+ Ticket</a>
</div>

<!-- CONTAINERS -->
<div class='container1'>
    <?php
        // Build items from the 'ToDo' directory 
        BuildItems('ToDo');
    ?>
</div>

<div class='container2'>
    <?php
        // Build items from the 'InProgress' directory 
        BuildItems('InProgress');
    ?>
</div>

<div class='container3'>
    <?php
        // Build items from the 'Complete' directory 
        BuildItems('Complete');
    ?>
</div>

</body>

</html>