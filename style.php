/*
*
*   FredMan - Project Manager
*   2019 - David Collins-Cubitt
*
*/

body {
    color: #262626;
    background-color: #fff;
    font-family: helvetica, arial, sans;
    padding:0px;
}

div.menu { 
    padding: 10px;
    color: #fff;
    background-color: #606; 
    width: 100%;
    position: fixed;
    top:0px;
    left:0px;
}

a.NewTicket {
    color: #fff;
    position: fixed;
    top: 10px;
    right: 10px;
    font-weight: bold;
    text-decoration: none;
}

div.logo {
    font-weight: bold;
}

div.header {
    font-weight: bold;
    text-align: center;
}

div.container1 {
    width:32%;
    display:inline-block;
    margin-top:40px;
}

div.container2 {
    width:32%;
    padding-left: 5px;
    display:inline-block;
    margin-top: 40px;
}

div.container3 {
    width:32%;
    padding-left: 5px;
    display: inline-block;
    margin-top: 40px;
}

div.ticket-ToDo {
    background-color: #eddada;
    padding: 2px;
    float: left;
    width: 100%;
    margin-top: 10px;
    border-radius: 8px;
}

div.ticket-InProgress {
    background-color: #edecda;
    padding: 2px;
    float: left;
    width: 100%;
    margin-top: 10px;
    border-radius: 8px;
}

div.ticket-Complete {
    background-color: #d4ebd3;
    padding: 2px;
    float: left;
    width: 100%;
    margin-top: 10px;
    border-radius: 8px;
}

/* Buttons */
input.delete {
    background-color: #a61b1b;
    color: #ffffff;
    border-radius: 100px;
    height: 26px;
    width: 26px;
    font-weight: bold;
    font-size: 10px;
    outline: none;
    border: 0;
    float: right;
    padding: 0;
    margin: 5px;
    text-align: center;
}

div.back-forward {
    margin: auto;
    margin-top:20px;
    width: 100px;
}

/* New Ticket Form */
div.form {
    font-family: helvetica, arial, sans;
    position: relative;
    padding: 30px;
    color: <?php echo $textcolour; ?>;
    border-radius: 10px;
    background-color: #ffffff;
    box-shadow: 5px 5px 10px #9aa5ad;
    margin: 0 auto;
    margin-top: 120px;
    width: 500px;
}

input.box {
    width: 100%;
    outline: 2px;
    outline-color: #000000;
    background-color: #e0e3e5;
    padding: 10px;
    border: 0;
    border-radius: 4px;
    color: <?php echo $textcolour; ?>;
    font-weight: bold;
    font-family: helvetica, arial, sans;
}

textarea {
    width: 100%;
    outline: 2px;
    outline-color: #000000;
    background-color: #e0e3e5;
    padding: 10px;
    border: 0;
    border-radius: 4px;
    color: <?php echo $textcolour; ?>;
    font-weight: bold;
    font-family: helvetica, arial, sans;
}

input.ticketsummary {
    cursor: pointer;
    width: 100%;
    padding: 0px;
    border: 0;
    background-color: #ffffff;
    color: <?php echo $textcolour; ?>;
    font-weight: bold;
    font-family: helvetica, arial, sans;
    text-align: left;
  }

input.submit {
    width: 100%;
    outline: none;
    background-color: <?php echo $maincolour; ?>;
    padding: 10px;
    border: 0;
    border-radius: 4px;
    color: <?php echo $buttontext; ?>;
    font-weight: bold;
    font-family: helvetica, arial, sans;
  }