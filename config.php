<?php

// Website Name
$SiteName = 'FredMan - Project Manager';

// Where is the site?
$AppLocation = '/var/www/html/FredMan';

// Developer Mode
$HideErrors = true; // True or False

// Update page every: 10 seconds
$RefreshTime= '10';

?>