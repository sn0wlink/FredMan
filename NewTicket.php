<?php
    // Includes
    include('variables.php');
    include('config.php');
    include('functions.php');
?>

<html>

<head>
    <title><?php echo $SiteName; ?></title>
    <link rel="stylesheet" type="text/css" href="style.php" />
</head>

<body>

<div class='menu'>
    <div class='logo'>
        <?php echo "$SiteName"; ?>
    </div>
        <a class='NewTicket' href='index.php'>< Back</a>
</div>

<div class='form'>
<form action="NewTicket.php" method="post">
    <input class='box' type="text" name="username" placeholder= "* Name" value=""/><br /><br />
    <input class='box' type="text" name="summary" placeholder="* Summary" value="" /><br /><br />
    <input class='submit' type="submit" name="submit" value="Send" />
</form>

</body>

</html>

<?php

// New Ticket Section

// Setup Variables
$username = '';
$problem = '';

if (isset($_POST['username'])) {
    $username = $_POST['username']; 
}

if (isset($_POST['summary'])) {
    $summary = $_POST['summary'];
}

// Write data to Ticket Layout
include 'ticket-layout.php';

if ($_POST) {
    
    // Gets date and time
    $datetime= date("ymd-His");

    // Open the folder and create file
    $ticketfile = fopen("ToDo/$datetime.txt", "w") or die("Failed!");

    // Write to file and and close
    fwrite($ticketfile, $ticket);
    fclose($ticketfile);

    // Load sucess page
    header('Location: index.php'); 
}

?>