<?php
function BuildItems($DirName) {
    echo "<div class='header'>$DirName</div>";

    foreach (glob("$DirName/*", GLOB_BRACE) as $file) {

    // Open each file
    $TicketContent = file_get_contents("$file");
    $TrimContent = explode("\n", "$TicketContent");
    $Summary = str_replace('Summary: ', '', $TrimContent[11]);
    $DisplayTicket = "$TrimContent[7]<br />$Summary";

    echo "<div class='ticket-$DirName'>";

    echo "<form action='functions.php' method='POST'>";
    echo "<input class='delete' type='submit' name='delete' value='X' />";
    echo "<input type='hidden' name='filename' value='$file'>";
    echo "<br />";
    echo $DisplayTicket;
    echo "<br />";
    echo "<div class='back-forward'>";
    echo "<input class='back' type='submit' name='Back-$DirName' value='<' />";
    echo "<input class='forward' type='submit' name='Forward-$DirName' value='>' />";
    echo "</div>";
    echo "</form>";
    echo "</div>";

    }
}

$filechosen = $_POST['filename'];

// Delete selected ticket
    if (isset($_POST["delete"])) {
        $path = realpath($filechosen);
        unlink($path);
        header("location: index.php");
    }

// Move ToDo to InProgress
    if (isset($_POST["Forward-ToDo"])) {
        $path = realpath($filechosen);
        $destination = str_replace("ToDo", "InProgress", $path);
        rename("$path", "$destination");
        header("location: index.php");
    }

// Move InProgress to Complete
    if (isset($_POST["Forward-InProgress"])) {
        $path = realpath($filechosen);
        $destination = str_replace("InProgress", "Complete", $path);
        rename("$path", "$destination");
        header("location: index.php");
    }

// Move Complete to InProgress
    if (isset($_POST["Back-Complete"])) {
        $path = realpath($filechosen);
        $destination = str_replace("Complete", "InProgress", $path);
        rename("$path", "$destination");
        header("location: index.php");
    }

// Move Inprogress to ToDo
    if (isset($_POST["Back-InProgress"])) {
        $path = realpath($filechosen);
        $destination = str_replace("InProgress", "ToDo", $path);
        rename("$path", "$destination");
        header("location: index.php");
    }

// Fixes buttons at start
    if (isset($_POST["Back-ToDo"])) {
        header("location: index.php");
    }

// Fixes buttons at end
    if (isset($_POST["Forward-Complete"])) {
        header("location: index.php");
    }

// Developer Mode
    if ($HideErrors == true) {
        // Turn off error reporting
        error_reporting(0);
    }

    else {
        // Display all errors
        error_reporting(E_ALL);
    }

?>